from typing import Any, Dict

try:
    import google.auth
    from google.auth.credentials import Credentials

    HAS_LIBS = (True,)
except ImportError as e:
    HAS_LIBS = False, str(e)


def __virtual__(hub):
    return HAS_LIBS


def __init__(hub):
    # Unlock acct by default if the virtual passed
    hub.acct.UNLOCKED = True


def gather(hub) -> Dict[str, Any]:
    """
    scopes (Sequence[str]): The list of scopes for the credentials. If
        specified, the credentials will automatically be scoped if
        necessary.
    request (google.auth.transport.Request): An object used to make
        HTTP requests. This is used to detect whether the application
        is running on Compute Engine. If not specified, then it will
        use the standard library http client to make requests.
    quota_project_id (Optional[str]):  The project ID used for
        quota and billing.
    """
    # Break early if a default has already been defined in encrypted acct credentials
    for k, v in hub.acct.PROFILES.items():
        if k.startswith("gcp"):
            if "default" in v:
                return {}

    ctx = {}
    try:
        credentials, project_id = google.auth.default(request=Credentials)
        ctx["default"] = {"credentials": credentials}
    except Exception as e:
        hub.log.debug(e)
    return ctx
