# https://cloud.google.com/resource-manager/docs/creating-managing-folders#python


def _wrap_crm_client(hub, target: str):
    def _resource_manager(ctx, *args, **kwargs):
        service = hub.exec.gcp.crm.init.service(ctx)
        with service.folders() as folders:
            request = getattr(folders, target)(*args, **kwargs)
            return request.execute()

    return _resource_manager


def __func_alias__(hub):
    out = {}
    for func in (
        "clearOrgPolicy",
        "listOrgPolicies",
        "getOrgPolicy",
        "listAvailableOrgPolicyConstraints",
        "setOrgPolicy",
    ):
        out[func] = _wrap_crm_client(hub, func)
    return out
