def _wrap_crm_client(hub, target: str):
    def _resource_manager(ctx, *args, **kwargs):
        service = hub.exec.gcp.crm.init.service(ctx)
        with service.operations() as operations:
            request = getattr(operations, target)(*args, **kwargs)
            return request.execute()

    return _resource_manager


def __func_alias__(hub):
    out = {}
    for func in ("get",):
        out[func] = _wrap_crm_client(hub, func)
    return out
