# https://cloud.google.com/resource-manager/docs/creating-managing-organization#python


def _wrap_crm_client(hub, target: str):
    def _resource_manager(ctx, *args, **kwargs):
        service = hub.exec.gcp.crm.init.service(ctx)
        with service.organizations() as org:
            request = getattr(org, target)(*args, **kwargs)
            return request.execute()

    return _resource_manager


def __func_alias__(hub):
    out = {}
    for func in (
        "listAvailableOrgPolicyConstraints",
        "get",
        "getOrgPolicy",
        "getIamPolicy",
        "testIamPermissions",
        "search",
        "setIamPolicy",
        "listOrgPolicies",
        "getEffectiveOrgPolicy",
        "clearOrgPolicy",
        "setOrgPolicy",
        "listAvailableOrgPolicyConstraints_next",
        "search_next",
        "listOrgPolicies_next",
    ):
        out[func] = _wrap_crm_client(hub, func)

    return out
