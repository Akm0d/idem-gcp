try:
    import googleapiclient.discovery as discovery

    HAS_LIBS = (True,)
except ImportError as e:
    HAS_LIBS = False, str(e)


def __virtual__(hub):
    return HAS_LIBS


def build(hub, ctx, service_name: str):
    # TODO load thes from the acct plugin (and ctx.acct) or hub.OPT
    api_version = "v1"

    ret = discovery.build(
        serviceName=service_name, version=api_version, credentials=ctx.acct.credentials,
    )

    return ret
